- I'm using the master branch because I felt no need to use a separated one.
- I'm not using a Money value object because the solution is too simple.

### todo
- [x] Refactor CartTest
- [x] Refactor Repository classes to do not load data on construct. Lazy-load
- [x] Create Abstract repository
- [x] Remove duplication on Repository tests
- [x] Document all classes and methods
- Create ProductRepository::get($id);
- Add functional tests

###  Install instructions
- `vagrant up`
- `composer install`
- Add `192.168.33.10   lightspeed.dev` to /etc/hosts
- Go to `http://lightspeed.dev/` and voilà!
<?php
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Session\Session;
use Lightspeed\eCom\ProductRepository;
use Lightspeed\eCom\AddressRepository;
use Lightspeed\eCom\Cart\Cart;
use Lightspeed\eCom\Cart\SessionStorage;

$app = new Silex\Application();
$app->register(new Silex\Provider\TwigServiceProvider(), ['twig.path' => __DIR__ . '/../app/views',]);
$app->register(new Silex\Provider\SessionServiceProvider());

$app['product.repository'] = new ProductRepository(__DIR__ . '/../data/products.json');
$app['address.repository'] = new AddressRepository(__DIR__ . '/../data/addresses.json');
$app['cart'] = new Cart(new SessionStorage(new Session()));
$app['debug'] = true;

return $app;
<?php
/**
 * Application bootstrap.
 * This file serves as bootstrap for different interfaces (web, test, cli).
 * Any initialization should be stored there.
 */
require_once __DIR__ . '/../vendor/autoload.php';

<?php
declare (strict_types = 1);

namespace Lightspeed\eCom;

use PHPUnit\Framework\TestCase;

class ProductRepositoryTest extends TestCase
{
    /**
     * @var ProductRepository
     */
    protected $repository;

    public function setUp()
    {
        $this->repository = new ProductRepository(__DIR__ . '/../data/products.json');
    }

    public function testFind()
    {
        $this->assertEquals(new Product(1, 'Product 1', 10.00), $this->repository->find(1));
    }

    public function testFindMustReturnNull()
    {
        $this->assertNull($this->repository->find(5));
    }

    public function testFindAll()
    {
        $this->assertEquals($this->buildProducts(), $this->repository->findAll());
    }

    /**
     * @return array
     */
    private function buildProducts()
    {
        $products = [];
        foreach (range(1, 4) as $i) {
            $products[$i] = new Product($i, "Product {$i}", $i * 10);
        }
        return $products;
    }
}

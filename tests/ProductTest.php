<?php
declare (strict_types = 1);

namespace Lightspeed\eCom;

use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testConstructor()
    {
        $id = 1;
        $name = 'Foo';
        $price = 3.58;
        $product = new Product($id, $name, $price);

        $this->assertAttributeEquals($id, 'id', $product);
        $this->assertAttributeEquals($name, 'name', $product);
        $this->assertAttributeEquals($price, 'price', $product);
    }
}

<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

use PHPUnit\Framework\TestCase;

class CouponRepositoryTest extends TestCase
{
    /**
     * @var CouponRepositoryTest
     */
    protected $repository;

    public function setUp()
    {
        $this->repository = new CouponRepository(__DIR__ . '/../../data/coupons.json');
    }

    public function testFind()
    {
        $this->assertEquals(new Coupon("XPTO", 10.00), $this->repository->find("XPTO"));
    }

    public function testFindMustReturnNull()
    {
        $this->assertNull($this->repository->find('ABC'));
    }
}

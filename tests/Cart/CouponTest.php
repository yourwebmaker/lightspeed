<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

use PHPUnit\Framework\TestCase;

class CouponTest extends TestCase
{
    public function testConstructor()
    {
        $code = 'XPTO';
        $discount = 10.00;
        $coupon = new Coupon($code, $discount);

        $this->assertAttributeEquals($code, 'code', $coupon);
        $this->assertAttributeEquals($discount, 'discount', $coupon);
    }
}

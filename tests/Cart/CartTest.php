<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

use PHPUnit\Framework\TestCase;
use Lightspeed\eCom\Product;
use Prophecy\Prophecy\ObjectProphecy;

class CartTest extends TestCase
{
    /**
     * @var StorageInterface|ObjectProphecy
     */
    protected $storage;
    /**
     * @var Cart
     */
    protected $cart;
    /**
     * @var Item
     */
    protected $item;
    /**
     * @var int
     */
    protected $index = 1;

    public function setUp()
    {
        $this->storage = $this->prophesize(StorageInterface::class);
        $this->item = $this->createItem($this->index, 10.00, 2);

        $this->items = [
            $this->item,
            $this->createItem(2, 30.00, 3),
            $this->createItem(3, 45.00, 2)
        ];

        $this->cart = new Cart($this->storage->reveal());
    }

    public function testAddItem()
    {
        $this->storage->add($this->item)->shouldBeCalled();
        $this->cart->addItem($this->item);
    }

    public function testRemoveItem()
    {
        $index = 1;
        $this->storage->remove($index)->shouldBeCalled();
        $this->cart->removeItem($index);
    }

    public function testChangeQuantity()
    {
        $newQuantity = 10;
        $this->storage->update($this->index, $this->item)->shouldBeCalled();
        $this->storage->get($this->index)->willReturn($this->item)->shouldBeCalled();
        $this->cart->changeQuantity($this->index, $newQuantity);
        $this->assertEquals($newQuantity, $this->item->getQuantity());
    }

    public function testGetTotal()
    {
        $this->storage->all()->willReturn($this->items)->shouldBeCalled();
        $this->assertEquals(200.00, $this->cart->getTotal());
    }

    public function testGetTotalWithCoupon()
    {
        $this->storage->all()->willReturn($this->items)->shouldBeCalled();
        $coupon = new Coupon('XPTO', 20.00);
        $this->assertEquals(180.00, $this->cart->getTotal($coupon));
    }

    public function testGetNumberOfItems()
    {
        $this->storage->all()->willReturn($this->items)->shouldBeCalled();
        $this->assertEquals(7, $this->cart->getNumberOfItems());
    }

    public function testGetItems()
    {
        $this->storage->all()->willReturn($this->items)->shouldBeCalled();
        $this->assertEquals($this->items, $this->cart->getItems());
    }

    public function testClear()
    {
        $this->storage->clear()->shouldBeCalled();
        $this->cart->clear();
    }
    
    /**
     * @param int $productId
     * @param float $price
     * @param int $quantity
     * @return Item
     */
    private function createItem(int $productId, float $price, $quantity = 1)
    {
        return new Item(new Product($productId, "Product-{$productId}", $price), $quantity);
    }
}

<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

use PHPUnit\Framework\TestCase;
use Lightspeed\eCom\Product;

class ItemTest extends TestCase
{
    public function testConstructor()
    {
        $product = new Product(1, 'Foo', 3.50);
        $quantity = 10;
        $item = new Item($product, 10);

        $this->assertAttributeEquals($product, 'product', $item);
        $this->assertAttributeEquals($quantity, 'quantity', $item);
    }

    public function testGetSubTotal()
    {
        $product = new Product(1, 'Foo', 3.50);
        $item = new Item($product, 10);
        $subTotal = 35;

        $this->assertEquals($subTotal, $item->getSubTotal());
    }

    public function testChangeQuantity()
    {
        $product = new Product(1, 'Foo', 3.50);
        $item = new Item($product, 10);
        $newQuantity = 20;

        $item->changeQuantity($newQuantity);

        $this->assertEquals($newQuantity, $item->getQuantity());
    }
}

<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Lightspeed\eCom\Product;

class SessionStorageTest extends TestCase
{
    /**
     * @var SessionStorage
     */
    protected $storage;

    public function setUp()
    {
        $session = new Session(new MockArraySessionStorage());
        $this->storage = new SessionStorage($session);
    }

    public function testAddItem()
    {
        $productId = 1;
        $price = 30.00;
        $quantity = 2;
        $item = $this->createItem($productId, $price, $quantity);
        
        $this->storage->add($item);
        $this->assertContains($item, $this->storage->all());
    }
    
    public function testUpdateItem()
    {
        $productId = 1;
        $price = 30.00;
        $quantity = 2;
        $item = $this->createItem($productId, $price, $quantity);

        $this->storage->add($item);
        $newItem = $this->createItem($productId, $price, 3);

        $this->storage->update($productId, $newItem);

        $this->assertEquals($newItem, $this->storage->get($productId));
    }

    /**
     * @param int $productId
     * @param float $price
     * @param int $quantity
     * @return Item
     */
    private function createItem(int $productId, float $price, $quantity = 1)
    {
        return new Item(new Product($productId, "Product-{$productId}", $price), $quantity);
    }
}

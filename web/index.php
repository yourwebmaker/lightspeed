<?php
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$app = require_once __DIR__ . '/../app/app.php';

/**
 * Home page handler.
 */
$app->get('/', function () use ($app) {
    return $app['twig']->render('home.twig', [
        'products' => $app['product.repository']->findAll(),
    ]);
});

/**
 * POST method that receives the 'Add to Cart' request.
 */
$app->post('/cart', function (Request $request) use ($app) {
    $id = (int) $request->request->get('id');
    $quantity = (int) $request->request->get('quantity');
    $product = $app['product.repository']->find($id);

    $app['cart']->addItem(new \Lightspeed\eCom\Cart\Item($product, $quantity));
    $app['session']->getFlashBag()->add('success', "The product has '{$product->getName()}' been added to your cart");

    return $app->redirect('/');
});

/**
 * Displays the cart page.
 */
$app->get('/cart', function () use ($app) {
    return $app['twig']->render('cart.twig', [
        'cart' => $app['cart'],
    ]);
});

/**
 * Removes an Item from the cart.
 */
$app->get('/cart/remove/{index}', function ($index) use ($app) {
    $product = $app['product.repository']->find($index);
    $app['cart']->removeItem($index);
    $app['session']->getFlashBag()->add('success', "The product has '{$product->getName()}' been removed from your cart");

    return $app->redirect('/cart');
});

/**
 * Process the user request for change quantity from an Item on the cart.
 */
$app->post('/cart/update-quantity/{index}', function ($index, Request $request) use ($app) {
    $quantity = (int) $request->request->get('quantity');
    $app['cart']->changeQuantity((int) $index, $quantity);
    $app['session']->getFlashBag()->add('success', 'Your cart has been updated');
    return $app->redirect('/cart');
});

/**
 * Displays the checkout page.
 */
$app->get('/checkout', function () use ($app) {
    $countries = json_decode(file_get_contents(__DIR__ . '/../data/countries.json'), true);
    return $app['twig']->render('checkout.twig', [
        'cart'      => $app['cart'],
        'countries' => $countries,
    ]);
});

/**
 * Process the addresses sent by the user.
 */
$app->post('/checkout/process', function (Request $request) use ($app) {
    $addressesData = $request->request->get('address');
    foreach ($addressesData as $type => $data) {
        $address = new \Lightspeed\eCom\Address(
            $data['street'],
            (int)$data['postalCode'],
            $data['city'],
            $data['country'], $type
        );
        $app['address.repository']->save($address);
    }

    $app['cart']->clear();
    $app['session']->getFlashBag()->add('success', 'Thank You! Your order is being processed');
    return $app->redirect('/');
});

$app->run();

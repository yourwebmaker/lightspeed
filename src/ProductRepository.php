<?php
declare (strict_types = 1);

namespace Lightspeed\eCom;

class ProductRepository extends AbstractJsonRepository implements ProductRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function find(int $productId)
    {
        $found = array_filter($this->getData(), function ($item) use ($productId) {
            return $item['id'] === $productId;
        });

        if (count($found) === 0) {
            return null;
        }

        $found = array_shift($found);

        return new Product($found['id'], $found['name'], $found['price']);
    }

    /**
     * @inheritdoc
     */
    public function findAll() : array
    {
        $products = [];
        foreach ($this->getData() as $item) {
            $products[$item['id']] = $this->hydrateObject($item);
        }
        return $products;
    }

    /**
     * @inheritdoc
     */
    protected function hydrateObject(array $data)
    {
        return new Product($data['id'], $data['name'], $data['price']);
    }
}

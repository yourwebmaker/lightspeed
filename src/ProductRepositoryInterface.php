<?php
declare (strict_types = 1);

namespace Lightspeed\eCom;

/**
 * Interface ProductRepositoryInterface
 *
 * Locates and persists products into the system.
 * 
 * @package Lightspeed\eCom
 */
interface ProductRepositoryInterface
{
    /**
     * Finds a Product inside the system.
     *
     * @param int $productId
     * @return Product|null
     */
    public function find(int $productId);

    /**
     * Finds all Product inside the system.
     *
     * @return array|Product[]
     */
    public function findAll() : array;
}

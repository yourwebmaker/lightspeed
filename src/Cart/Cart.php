<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

/**
 * Class Cart
 *
 * Service class that holds all cart functionality.
 *
 * @package Lightspeed\eCom
 */
final class Cart
{
    /**
     * @var StorageInterface
     */
    private $storage;

    /**
     * Cart constructor.
     * @param StorageInterface $storage
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Adds an Item into the Cart.
     *
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->storage->add($item);
    }

    /**
     * Removes an Item from the stack by its index.
     *
     * @param int $index
     */
    public function removeItem(int $index)
    {
        $this->storage->remove($index);
    }

    /**
     * Updates a quantity of an Item by its index.
     *
     * @param int $index
     * @param int $newQuantity
     */
    public function changeQuantity(int $index, int $newQuantity)
    {
        $newItem = $this->storage->get($index);
        $newItem->changeQuantity($newQuantity);
        $this->storage->update($index, $newItem);
    }

    /**
     * Gets the sum of all Items inside the Cart.
     * If a Coupon was provided, it applies the discount from the Coupon.
     *
     * @param Coupon $coupon
     * @return float
     */
    public function getTotal(Coupon $coupon = null) : float
    {
        $total = array_reduce($this->storage->all(), function ($i, Item $item) {
            return $i += $item->getSubTotal();
        }, 0);

        if ($coupon) {
            $total -= $coupon->getDiscount();
        }

        return $total;
    }

    /**
     * Returns the number of items.
     *
     * The sum is made on SUM(Items * Quantity).
     *
     * @return int
     */
    public function getNumberOfItems() : int
    {
        return array_reduce($this->storage->all(), function ($i, Item $item) {
            return $i += $item->getQuantity();
        }, 0);
    }

    /**
     * Returns all Items inside the Cart.
     *
     * @return array|Item[]
     */
    public function getItems() : array
    {
        return $this->storage->all();
    }

    /**
     * Removes all items inside the cart.
     */
    public function clear()
    {
        $this->storage->clear();
    }
}

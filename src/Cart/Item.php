<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

use Lightspeed\eCom\Product;

/**
 * Class Item
 * 
 * Represents an Item inside the cart.
 * 
 * @package Lightspeed\eCom\Cart
 */
final class Item
{
    /**
     * The product which this Item represents.
     * 
     * @var Product
     */
    private $product;
    /**
     * The quantity of the items bought by the customer.
     * 
     * @var int
     */
    private $quantity;

    /**
     * Item constructor.
     * @param $product
     * @param $quantity
     */
    public function __construct(Product $product, int $quantity = 1)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getSubTotal() : float
    {
        return $this->product->getPrice() * $this->quantity;
    }

    /**
     * @return int
     */
    public function getQuantity() : int
    {
        return $this->quantity;
    }

    /**
     * @return Product
     */
    public function getProduct() : Product
    {
        return $this->product;
    }

    /**
     * Changes the item's quantity.
     * 
     * @param int $newQuantity
     */
    public function changeQuantity(int $newQuantity)
    {
        $this->quantity = $newQuantity;
    }
}

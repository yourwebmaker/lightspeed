<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

/**
 * Interface CouponRepositoryInterface
 * 
 * Locates coupons inside the system.
 * 
 * @package Lightspeed\eCom\Cart
 */
interface CouponRepositoryInterface
{
    /**
     * Tries to find a Coupon by its code.
     * 
     * @param string $code
     * @return Coupon|null
     */
    public function find(string $code);
}

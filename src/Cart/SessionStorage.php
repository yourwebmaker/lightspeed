<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class SessionStorage
 * 
 * HTTP Session based implementation of the cart storage.
 * 
 * Allows the cart the persist the cart data through the pages.
 * 
 * @package Lightspeed\eCom\Cart
 */
class SessionStorage implements StorageInterface
{
    /**
     * OOP based $_SESSION component.
     * 
     * @var SessionInterface
     */
    private $session;

    /**
     * SessionStorage constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @inheritDoc
     */
    public function add(Item $item)
    {
        $this->session->set($item->getProduct()->getId(), $item);
    }

    /**
     * @inheritDoc
     */
    public function remove(int $index)
    {
        $this->session->remove($index);
    }

    /**
     * @inheritDoc
     */
    public function all() : array
    {
        return $this->session->all();
    }

    /**
     * @inheritDoc
     */
    public function update(int $index, Item $newItem)
    {
        $this->session->set($index, $newItem);
    }

    /**
     * @inheritDoc
     */
    public function get(int $index) : Item
    {
        return $this->session->get($index);
    }

    /**
     * @inheritDoc
     */
    public function clear()
    {
        $this->session->clear();
    }
}

<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

/**
 * Class Coupon
 *
 * Just a class that stores coupon data.
 * 
 * @package Lightspeed\eCom\Cart
 */
final class Coupon
{
    /**
     * Unique code of the coupon.
     * 
     * @var string
     */
    private $code;
    /**
     * The amount that will be decreased on the total in case this coupon is used.
     * 
     * @var float
     */
    private $discount;

    /**
     * Coupon constructor.
     * @param string $code
     * @param float $discount
     */
    public function __construct(string $code, float $discount)
    {
        $this->code = $code;
        $this->discount = $discount;
    }

    /**
     * @return float
     */
    public function getDiscount() : float
    {
        return $this->discount;
    }
}

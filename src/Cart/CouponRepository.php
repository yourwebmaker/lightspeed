<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

use Lightspeed\eCom\AbstractJsonRepository;

/**
 * Class CouponRepository
 * 
 * JSON/File-based implementation of CouponRepositoryInterface.
 * 
 * @package Lightspeed\eCom\Cart
 */
class CouponRepository extends AbstractJsonRepository implements CouponRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function find(string $code)
    {
        $found = array_filter($this->getData(), function ($item) use ($code) {
            return $item['code'] === $code;
        });

        if (count($found) === 0) {
            return null;
        }

        $found = array_shift($found);

        return new Coupon($found['code'], $found['discount']);
    }

    /**
     * @inheritDoc
     */
    protected function hydrateObject(array $data)
    {
        return new Coupon($data['code'], $data['discount']);
    }
}

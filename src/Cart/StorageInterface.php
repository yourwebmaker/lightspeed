<?php
declare (strict_types = 1);

namespace Lightspeed\eCom\Cart;

/**
 * Interface StorageInterface
 *
 * Rule the Cart's storage methods.
 *
 * @package Lightspeed\eCom\Cart
 */
interface StorageInterface
{
    /**
     * Adds an Item into the storage.
     *
     * @param Item $item
     * @return void
     */
    public function add(Item $item);

    /**
     * Removes and Item from the storage by its index on the stack.
     *
     * @param $index
     * @return void
     */
    public function remove(int $index);

    /**
     * Returns all Items.
     * 
     * @return Item[]
     */
    public function all() : array;

    /**
     * Update an Item by its index.
     * 
     * @param int $index
     * @param Item $item
     * @return void
     */
    public function update(int $index, Item $item);

    /**
     * Returns an item by its index.
     * 
     * @param $index
     * @return Item
     */
    public function get(int $index) : Item;

    /**
     * Removes all items.
     * 
     * @return void
     */
    public function clear();
}

<?php
declare (strict_types = 1);

namespace Lightspeed\eCom;

/**
 * Class AbstractJsonRepository
 * 
 * Helper methods the ensure children classes will work correctly.
 * 
 * @package Lightspeed\eCom
 */
abstract class AbstractJsonRepository
{
    /**
     * Json data converted into PHP array.
     * 
     * @var array
     */
    protected $data;
    /**
     * Json's file location.
     * 
     * @var string
     */
    protected $filename;

    /**
     * AbstractJsonRepository constructor.
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * Returns the array representation from a json file.
     * 
     * @return array
     */
    protected function getData() : array
    {
        return json_decode(file_get_contents($this->filename), true);
    }

    /**
     * Forces the user the implement an array => Object implementation 
     * that will be used by repositories classes.
     * 
     * @param array $data
     * @return mixed
     */
    abstract protected function hydrateObject(array $data);
}

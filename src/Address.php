<?php
declare (strict_types = 1);

namespace Lightspeed\eCom;

/**
 * Class Address
 * 
 * Just holds address data.
 * 
 * @package Lightspeed\eCom
 */
class Address
{
    /**
     * @var string
     */
    private $street;
    /**
     * @var int
     */
    private $postalCode;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $country;
    /**
     * @var string
     */
    private $type;

    /**
     * Address constructor.
     * @param string $street
     * @param int $postalCode
     * @param string $city
     * @param string $country
     * @param string $type
     */
    public function __construct(
        string $street,
        int $postalCode,
        string $city,
        string $country,
        string $type)
    {
        $this->street = $street;
        $this->postalCode = $postalCode;
        $this->city = $city;
        $this->country = $country;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return int
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

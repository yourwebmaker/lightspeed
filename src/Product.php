<?php
declare (strict_types = 1);

namespace Lightspeed\eCom;

/**
 * Class Product
 * 
 * Just holds product data.
 * 
 * @package Lightspeed\eCom
 */
final class Product
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var float
     */
    private $price;

    /**
     * Product constructor.
     * @param $id
     * @param $name
     * @param $price
     */
    public function __construct(int $id, string $name, float $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }
}

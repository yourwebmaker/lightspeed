<?php
declare (strict_types = 1);

namespace Lightspeed\eCom;

class AddressRepository extends AbstractJsonRepository
{
    /**
     * @param Address $address
     */
    public function save(Address $address)
    {
        $addresses = $this->getData();
        $addresses[] = [
            'street'     => $address->getStreet(),
            'postalCode' => $address->getPostalCode(),
            'city'       => $address->getCity(),
            'country'    => $address->getCountry(),
            'type'       => $address->getType(),
        ];
        
        $whitten = file_put_contents($this->filename, json_encode($addresses));

        if ($whitten === false) {
            throw new \RuntimeException('Error while saving the address');
        }
    }

    /**
     * @inheritdoc
     */
    public function findAll() : array
    {
        $products = [];
        foreach ($this->getData() as $item) {
            $products[$item['id']] = $this->hydrateObject($item);
        }
        return $products;
    }

    /**
     * @inheritdoc
     */
    protected function hydrateObject(array $data)
    {
        return new Product($data['id'], $data['name'], $data['price']);
    }
}
